package com.megaprime;

import com.megaprime.engine.FullMegaPrimeFinder;

public class Main {

	// TASK
	// The task is to find the LARGEST full mega-prime less than ten million.
	//
	// EXPLANATION
	// A full mega prime is prime number that when you successively remove
	// digits
	// from either end of the prime number, you are left with a new prime
	// number;
	// for example, the number 997 is called a left-mega prime as the
	// numbers 997, 97, and 7 are all prime.

	// The number 7393 is a right-mega prime as
	// the numbers 7393, 739, 73, and 7 formed by removing digits from
	// its right are also prime.

	// A 'full' mega prime is one that is both left and right
	// E.g. 31 is full mega prime as it's both a left and right mega prime.
	// No zeroes are allowed in mega-primes.

	public static void main(String[] args) {
		FullMegaPrimeFinder finder = new FullMegaPrimeFinder();
		System.out.println("LARGEST full mega-prime less than ten million: "
				+ finder.findMax(10000000));

	}

}
