package com.megaprime.engine;

import com.megaprime.engine.MegaPrimeFactory.MEGA_TYPE;

public class FullMegaPrimeFinder {
	/**
	 * @throws IllegalArgumentException if you pass a negative number.
	 * @param number 
	 * @return largest possible number less than the argument
	 */
	public int findMax(int number) {
		if(number < 0)
		{
			throw new IllegalArgumentException("Invalid argument, use a positive number");
		}
		boolean megaPrime = MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL)
				.isMegaPrime(number);
		while (!megaPrime) {
			megaPrime = MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL)
					.isMegaPrime(--number);
		}
		return number;
	}

}
