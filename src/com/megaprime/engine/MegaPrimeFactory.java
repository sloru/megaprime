package com.megaprime.engine;

public class MegaPrimeFactory {
	
	public enum MEGA_TYPE{LEFT,RIGHT,FULL};

	public static IMegaPrime getMegaPrime(MEGA_TYPE type) {
		IMegaPrime prime = null;
		switch (type) {
		case LEFT:
			prime = LeftMegaPrime.getInstance();
			break;
		case RIGHT:
			prime = RightMegaPrime.getInstance();
			break;
		case FULL:
			prime = FullMegaPrime.getInstance();
			break;
		}
		return prime;
	}
}
