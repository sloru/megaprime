package com.megaprime.engine;

public interface IMegaPrime {
	
	/**
	 * @param number
	 * @return true if number is a MegaPrime
	 */
	public boolean isMegaPrime(int number);
	
}
