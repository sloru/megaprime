package com.megaprime.engine;

import com.megaprime.engine.MegaPrimeFactory.MEGA_TYPE;

public class FullMegaPrime implements IMegaPrime {

	private static FullMegaPrime instance;

	private FullMegaPrime() {
	}

	public static FullMegaPrime getInstance() {
		if (instance == null) {
			synchronized (FullMegaPrime.class) {
				if (instance == null) {
					instance = new FullMegaPrime();
				}
			}
		}
		return instance;
	}

	/**
	 * @throws IllegalArgumentException
	 *             if you pass a negative number.
	 * @param number
	 * @return true if number is a FullMegaPrime
	 */
	@Override
	public boolean isMegaPrime(int number) {
		if (number < 0) {
			throw new IllegalArgumentException("Invalid argument, use a positive number");
		}
		IMegaPrime leftMegaPrime = MegaPrimeFactory
				.getMegaPrime(MEGA_TYPE.LEFT);
		IMegaPrime rightMegaPrime = MegaPrimeFactory
				.getMegaPrime(MEGA_TYPE.RIGHT);
		return leftMegaPrime.isMegaPrime(number)
				&& rightMegaPrime.isMegaPrime(number);
	}

}
