package com.megaprime.engine;

import com.megaprime.util.PrimeUtils;

public class RightMegaPrime implements IMegaPrime {
	
	private static RightMegaPrime instance;

	private RightMegaPrime() {
	}

	public static RightMegaPrime getInstance() {
		if (instance == null) {
			synchronized (RightMegaPrime.class) {
				if (instance == null) {
					instance = new RightMegaPrime();
				}
			}
		}
		return instance;
	}

	/**
	 * @throws IllegalArgumentException
	 *             if you pass a negative number.
	 * @param number
	 * @return true if number is a RightMegaPrime
	 */
	@Override
	public boolean isMegaPrime(int number) {
		if(number < 0)
		{
			throw new IllegalArgumentException("Invalid argument, use a positive number");
		}
		boolean val = true;
		String string = Integer.toString(number);
		return isMegaPrimeRecursion(string, string.length() - 1, val);
	}

	/*
	 * Method that evaluates if a number is RightMegaPrime starting from the
	 * first digit.
	 */
	private boolean isMegaPrimeRecursion(String number, int increment,
			boolean val) {
		String newNumber = number.substring(0, number.length() - increment);
		if (!val || newNumber.isEmpty()) {
			return val;
		}

		if (newNumber.length() == number.length()) {
			return PrimeUtils.isPrime(Integer.parseInt(newNumber));
		}
		val = PrimeUtils.isPrime(Integer.parseInt(newNumber));
		return isMegaPrimeRecursion(number, --increment, val);
	}

}
