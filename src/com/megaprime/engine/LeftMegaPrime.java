package com.megaprime.engine;

import com.megaprime.util.PrimeUtils;

public class LeftMegaPrime implements IMegaPrime {

	private static LeftMegaPrime instance;

	private LeftMegaPrime() {
	}

	public static LeftMegaPrime getInstance() {
		if (instance == null) {
			synchronized (LeftMegaPrime.class) {
				if (instance == null) {
					instance = new LeftMegaPrime();
				}
			}
		}
		return instance;
	}
	
	/**
	 * @throws IllegalArgumentException
	 *             if you pass a negative number.
	 * @param number
	 * @return true if number is a LeftMegaPrime
	 */
	@Override
	public boolean isMegaPrime(int number) {
		if(number < 0)
		{
			throw new IllegalArgumentException("Invalid argument, use a positive number");
		}
		boolean val = true;
		String string = Integer.toString(number);
		return isMegaPrimeRecursion(string, 1, val);
	}

	/*
	 * Method that evaluates if a number is LeftMegaPrime starting from the last
	 * digit.
	 */
	private boolean isMegaPrimeRecursion(String number, int increment,
			boolean val) {
		String newNumber = number.substring(number.length() - increment,
				number.length());
		if (!val || newNumber.isEmpty()) {
			return val;
		}
		if (newNumber.length() == number.length()) {
			return PrimeUtils.isPrime(Integer.parseInt(newNumber));
		}
		val = PrimeUtils.isPrime(Integer.parseInt(newNumber));
		return isMegaPrimeRecursion(number, ++increment, val);
	}

}
