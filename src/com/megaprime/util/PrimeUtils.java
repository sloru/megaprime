package com.megaprime.util;

public class PrimeUtils {

	/**
	 * 
	 * @param number
	 * @return true if number is a prime number
	 */
	public static boolean isPrime(int number) {
		if(number < 0)
		{
			throw new IllegalArgumentException();
		}
		boolean val = true;
		if (number == 0)
			val = false;
		else if (number % 2 == 0 && number != 2)
			val = false;
		for (int i = 3; i * i <= number && val; i += 2) {
			if (number % i == 0)
				val = false;
		}
		return val;
	}

	public static int removeLeft(int number) {
		String stringNumber = Integer.toString(number);
		if (stringNumber.length() > 1) {
			stringNumber = stringNumber.substring(1);
		} else {
			return -1;
		}
		return Integer.parseInt(stringNumber);
	}

	public static int removeRight(int number) {
		String stringNumber = Integer.toString(number);
		if (stringNumber.length() > 1) {
			stringNumber = stringNumber.substring(0, stringNumber.length() - 1);
		} else {
			return -1;
		}
		return Integer.parseInt(stringNumber);
	}

}
